package com.kevinwu.server.servlet;

import com.kevinwu.server.utils.Request;
import com.kevinwu.server.utils.Response;

import java.io.InputStream;
import java.net.Socket;
import java.util.Map;

/**
 * 多线程执行Request请求
 */
public class RequestProcessor extends Thread {

    private Socket socket;
    //MyMiniCat加载的所有的应用的Servlet集合
    private Map<String,HttpServlet> servletMap;

    public RequestProcessor(Socket socket, Map<String, HttpServlet> servletMap) {
        this.socket = socket;
        this.servletMap = servletMap;
    }

    @Override
    public void run() {
        try{
            InputStream inputStream = socket.getInputStream();

            // 封装Request对象和Response对象
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());

            // 静态资源处理
            if(servletMap.get(request.getUrl()) == null) {
                response.outputHtml(request.getUrl());
            }else{
                // 动态资源servlet请求
                HttpServlet httpServlet = servletMap.get(request.getUrl());
                httpServlet.service(request,response);
            }
            socket.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
