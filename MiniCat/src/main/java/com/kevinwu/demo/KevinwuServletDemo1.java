package com.kevinwu.demo;

import com.kevinwu.server.servlet.HttpServlet;
import com.kevinwu.server.utils.HttpProtocolUtil;
import com.kevinwu.server.utils.Request;
import com.kevinwu.server.utils.Response;

import java.io.IOException;

/**
 * 测试应用demo1的servlet
 */
public class KevinwuServletDemo1 extends HttpServlet {
    @Override
    public void doGet(Request request, Response response) {

        String content = "<h1>Demo1 KevinwuServlet get</h1>";
        try {
            response.output((HttpProtocolUtil.getHttpHeader200(content.getBytes().length) + content));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(Request request, Response response) {
        String content = "<h1>Demo1 KevinwuServlet post</h1>";
        try {
            response.output((HttpProtocolUtil.getHttpHeader200(content.getBytes().length) + content));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws Exception {

    }

    @Override
    public void destory() throws Exception {

    }
}
