package com.kevinwu.server.servlet;

import com.kevinwu.server.utils.Request;
import com.kevinwu.server.utils.Response;

/**
 * HttpServlet方法 实现servlet接口
 */
public abstract class HttpServlet implements Servlet{

    /**
     * doGet方法
     * @param request
     * @param response
     */
    public abstract void doGet(Request request, Response response);

    /**
     * doPost方法
     * @param request
     * @param response
     */
    public abstract void doPost(Request request,Response response);


    /**
     * Service方法 根据method方法跳转到doGet方法或者doPost方法
     * @param request
     * @param response
     * @throws Exception
     */
    @Override
    public void service(Request request, Response response) throws Exception {
        if("GET".equalsIgnoreCase(request.getMethod())) {
            doGet(request,response);
        }else{
            doPost(request,response);
        }
    }
}
