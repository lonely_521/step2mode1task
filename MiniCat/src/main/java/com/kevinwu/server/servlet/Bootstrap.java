package com.kevinwu.server.servlet;

import com.kevinwu.server.utils.MyThreadPoolExecutor;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * MyMiniCat的主类 启动后启动socker监听http服务 以及将配置目录下的应用中所有的Servlet进行加载
 */
public class Bootstrap {

    /**
     * 定义MyMiniCat下部署应用中的所有Servlet集合
     */
    private Map<String,HttpServlet> servletMap = new HashMap<String,HttpServlet>();
    /**定义socket监听的端口号*/
    private int port = 8080;
    /**
     * 存放应用部署的目录绝对路径
     */
    private String appBase="";

    /**
     * Minicat启动需要初始化展开的一些操作
     */
    public void start() throws Exception {

        //1、加载server.xml文件  解析配置文件读取监听端口号(port)和部署目录的绝对路径(appBase)
        loadServerXml();
        //2、加载部署目录下的所有servlet到servlet映射集合中Mapper类servletMap
        servletMap = ServletMapper.getServletMapper(appBase);
        //3、启动Socket监听
        ServerSocket serverSocket = new ServerSocket(port);
        //多线程启动
        while(true) {
            Socket socket = serverSocket.accept();
            RequestProcessor requestProcessor = new RequestProcessor(socket,servletMap);
            ThreadPoolExecutor threadPoolExecutor = MyThreadPoolExecutor.getThreadPoolExecutor();
            threadPoolExecutor.execute(requestProcessor);
        }
    }

    /**
     * 加载server.xml文件
     */
    private void loadServerXml() {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("server.xml");
        SAXReader saxReader = new SAXReader();

        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();

            List<Element> selectNodes = rootElement.selectNodes("//Connector");
            for (int i = 0; i < selectNodes.size(); i++) {
                Element element =  selectNodes.get(i);
                String port = element.attribute("port").getValue();
                this.port = Integer.valueOf(port);
            }
            List<Element> selectNodes1 = rootElement.selectNodes("//Host");
            for (int i = 0; i < selectNodes1.size(); i++) {
                Element element =  selectNodes1.get(i);
                String name = element.attributeValue("name");
                String appBase = element.attributeValue("appBase");
                this.appBase = appBase;
            }

        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    /**
     * Minicat 的程序启动入口
     * @param args
     */
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        try {
            // 启动Minicat
            bootstrap.start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
