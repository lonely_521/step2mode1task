package com.kevinwu.server.servlet;

import com.kevinwu.server.utils.Request;
import com.kevinwu.server.utils.Response;

/**
 * servlet接口
 */
public interface Servlet {

    /**
     * 初始化方法
     * @throws Exception
     */
    void init() throws Exception;

    /**
     * 销毁方法
     * @throws Exception
     */
    void destory() throws Exception;

    /**
     * service方法
     * @param request
     * @param response
     * @throws Exception
     */
    void service(Request request, Response response) throws Exception;
}
